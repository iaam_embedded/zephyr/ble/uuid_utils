# Zephyr BLE UUID Utils

Single header library with c++ utilities to manipulate Zephyr OS BLE 128 bit uuids at compile time

## Requirements

- Zephyr OS v3.x
- C++ >=17
