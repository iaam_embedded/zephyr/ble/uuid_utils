/*!*****************************************************************
* Copyright 2022, Victor Chavez
* SPDX-License-Identifier: GPL-3.0-or-later
* @file uuid_utils.hpp
* @author Victor Chavez (chavez-bermudez@fh-aachen.de)
* @date Aug 17, 2022
*
* @brief
* C++ Utilities to manipulate UUIDs at compile time
*
* @par Dependencies
* - language: C++17
* - OS: Zephyr
********************************************************************/

#pragma once

#include <zephyr/bluetooth/uuid.h>
#include <stdint.h>

namespace uuid_utils
{
/** @brief Get the raw bytes of a bt_uuid_128 datatype
     @details Useful to initialize uuid in adv. data. <br>
	 		   Example: <br>
				static constexpr bt_data adv_data[] = 
				{
					BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL)),
					BT_DATA_BYTES(BT_DATA_UUID128_ALL,
								RAW_UUID_128(MY_128BIT_ZEPHYR_UUID)),
				}
    };
	@param UUID_128 128-bit UUID  bt_uuid_128 struct
 */
#define RAW_UUID_128(UUID_128) UUID_128.val[0],UUID_128.val[1],UUID_128.val[2],UUID_128.val[3], \
								 UUID_128.val[4],UUID_128.val[5],UUID_128.val[6],UUID_128.val[7], \
							     UUID_128.val[8],UUID_128.val[9],UUID_128.val[10],UUID_128.val[11], \
								 UUID_128.val[12],UUID_128.val[13],UUID_128.val[14],UUID_128.val[15]

/*!
	* @brief        Create a Derived UUID from a Base UUID
	*
	* @param [in]	base	The 128 bit uuid base
	* @param [in]	uuid_short The 16 bit short uuid 
	* @details      Replaces the 2 bytes previous to the end of the base UUID 
					Example
					
					Base UUID	6E400000-B5A3-F393-E0A9-E50E24DCCA9E
					Derived UUID 6E40xxxx-B5A3-F393-E0A9-E50E24DCCA9E

					where xxxx are the bytes replaced

	* @retval bt_uuid_128 UUID derived from base uuid
*/
constexpr bt_uuid_128 derive_uuid(bt_uuid_128 base,uint16_t uuid_short)
{
	const uint8_t uuid_short_msb = BT_UUID_SIZE_128-3;
	const uint8_t uuid_short_lsb = BT_UUID_SIZE_128-4;
	bt_uuid_128 derived_uuid = base;
	derived_uuid.val[uuid_short_msb] = uuid_short >> 8;
	derived_uuid.val[uuid_short_lsb] = uuid_short & 0xFF;
	return derived_uuid;
}
} // namespace uuid_utils

